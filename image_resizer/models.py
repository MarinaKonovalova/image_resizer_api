import base64
import io
from uuid import uuid4

from PIL import Image
from redis import Redis
from rq import Queue

r = Redis(host='127.0.0.1', port=6379, db=0)
q = Queue(connection=r)


class Images:
    @staticmethod
    def add_image(image_b64):
        image_id = uuid4().hex
        task = q.enqueue(Images.resize, image_b64, image_id)
        r.hmset('image:{}:original'.format(image_id), {
            "image": image_b64,
            "task_id": task.id,
        })
        return task.get_id(), task.get_status(), image_id

    @staticmethod
    def resize(image_b64, image_id):
        task_id = r.hgetall('image:{}:original'.format(image_id)).get('task_id')

        image_bytes = base64.b64decode(str(image_b64))
        image = Image.open(io.BytesIO(image_bytes))

        for size in [32, 64]:
            image_resized = image.resize((size, size), Image.ANTIALIAS)

            buffer = io.BytesIO()
            image_resized.save(buffer, format="PNG")
            image_resized = buffer.getvalue()

            image_resized_b64 = base64.b64encode(image_resized)

            r.hmset('image:{}:{}'.format(image_id, size), {
                "image": image_resized_b64,
                "task_id": task_id
            })

    @staticmethod
    def get_task(task_id):
        task = q.fetch_job(task_id)
        return task_id, task.get_status(), task.args[1]

    @staticmethod
    def get_image(image_id, size):
        image = r.hgetall('image:{}:{}'.format(image_id, size))
        return image_id, image.get('task_id'), image.get('image')
