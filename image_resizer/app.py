import typing as t
from http import HTTPStatus

from flask import Flask, request
from pydantic import ValidationError

from . import schemas
from .models import Images

app = Flask(__name__)


#@app.errorhandler(ValidationError)
#def handle_bad_request(error: ValidationError) -> t.Tuple[str, HTTPStatus]:
#    return error.json(), HTTPStatus.BAD_REQUEST


@app.route("/task", methods=["POST"])
def add_task():
    data: schemas.PostTaskRequest = schemas.PostTaskRequest.parse_raw(request.data)
    task_id, task_status, image_id = Images.add_image(data.image)
    return schemas.TaskResponse(task_id=task_id, status=task_status, image_id=image_id).json(), HTTPStatus.CREATED


@app.route('/task/<task_id>', methods=['GET'])
def get_task(task_id):
    task_id, task_status, image_id = Images.get_task(task_id)
    return schemas.TaskResponse(task_id=task_id, status=task_status, image_id=image_id).json(), HTTPStatus.OK


@app.route('/image/<image_id>', methods=['GET'])
def get_image(image_id):
    data: schemas.GetImageRequest = schemas.GetImageRequest.parse_obj(request.args)
    size = data.size
    image_id, task_id, image = Images.get_image(image_id, size)
    return schemas.ImageResponse(image_id=image_id, task_id=task_id, image=image).json(), HTTPStatus.OK
