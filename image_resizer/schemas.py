import pydantic as pd
import typing as t


class BaseModel(pd.BaseModel):
    class Config:
        orm_mode = True


class PostTaskRequest(BaseModel):
    image: bytes


class TaskResponse(BaseModel):
    task_id: str
    status: str
    image_id: str


class GetImageRequest(BaseModel):
    size: str


class ImageResponse(BaseModel):
    image_id: str
    task_id: str
    image: bytes
